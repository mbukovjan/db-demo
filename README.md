# DB demo application

This repo contains sample scripts necessary for building and deploying requested application to Openshift environment.

The scripts are not tested at the moment as the author does not have currently access to a working Openshift environment.

# Notes on Application 1

1. The application uses S2I build on the Openshift platform, specifically JBoss 72 EAP S2I builder image, containing OpenJDK11. 

It would be also possible to use non-S2I, Dockerfile builds, for which a base image should be created (sample Dockerfile is in samples
directory), however, using Red Hat supported base images is a preferred option on OCP platform as such build requires minimum code, maintenance and 
are considered safer as S2I build does not require root privileges on the host machines to run.

2. If JBoss would be an overkill, a more lightweight base image could be used - like openjdk18-openshift S2I images 

I also included a sample file to create a container from scratch using classic Dockerfile, for such build a different type of BuildConfig would be used.

The application is exposed on port 8080, and published via service and route objects from the OCP platform on the default URL (based on OCP cluster configuration
and location).

3. The application uses OpenJDK. Oracle JDK is not automatically downloadable from public resources, normally this component is distributed from private binary repos
inside the company, which I do not have.

4. The port 8080 is exposed. Openshift exposes the application inside the cluster via Service object, again on port 8080.

5. The application is also exposed outside via Route object, on standard 443 port (this is actually implemented via HAProxy instance in OCP), which acts as a proxy

6. The application is autoscaled to a maximum of 10 pods via HorizontalPodAutoscaler object. The scaling happens when a pod reaches 90% of maximum allocated capacity on CPU resource (which is set to 1 CPU)

# Notes on Application 2

1. Application is placed in src/app2 directory, and is written in Python. It uses "standard" Python libraries, I would say nothing of particular note (I did not install or use i.e. specialized OSM components).
It could be written also in Go - as a compiled language, it would be faster, but currently I do not have full Go development set up on my machine.

3. To save time, the application is containerized via S2I image as well. In this case, I use direct compile from source, so all the objects are auto-created automatically via Openshift premade objects. 
I expose the application on default hostname, this time in imperative way.

The application uses gunicorn framework to automatically determine environment to set up and run the application.

# General notices and remarks

Before deployment to a real cluster, a real cluster credentials must be provided in vars/cluster.yml file.

The code and associated objects serve only as a basis of discussion.

As I do not have access to a running Openshift instance, there may be syntax errors, typos and possibly wrong paths in the scripts; these scripts should serve as a demostration of concepts.

I tried to search for a demo OCP cluster on AWS but it is not available as a free service ATM (Openshift Dedicated evaluation can only be requested via Red hat sales team, which is an overkill for the purpose of this demo).

The deployment to OCP is handled by Ansible playbooks and roles, though normally we use Ansible only to provision certain aspects of OCP.

In my current environment, these tasks would be accomplished by Jenkins pipelines (e.g. primarily Groovy with Openshift DSL installed), either via dedicated Jenkins instance or Openshift pipelines. 
The code tends to be more expressive that way.

An OCP client and Python OCP/K8S libraries must be installed on the host machine.
