#!/bin/python

url = 'http://api.open-notify.org/iss-now.json'

import urllib2
import json
from datetime import datetime

def get_coordinates():

    req = urllib2.Request(url)
    response = urllib2.urlopen(req)

    obj = json.loads(response.read())

    return obj

def get_osm_url(position):

    latitude = position['latitude']
    longitude = position['longitude']

    return "http://www.openstreetmap.org/?mlat={0}&mlon={1}&zoom=12".format(latitude, longitude)

def get_message():
    coords = get_coordinates()
    maps_url = get_osm_url(coords['iss_position'])

    dt = datetime.fromtimestamp(coords['timestamp'])

    return 'Datetime: {0}\nMaps URL: {1}'.format(dt.strftime("%m/%d/%Y, %H:%M:%S"), maps_url)

def application(environ, start_response):
    start_response('200 OK', [('Content-Type','text/plain')])

    message = get_message()
    return [message.encode()]

if __name__ == "__main__":
    print get_message()
