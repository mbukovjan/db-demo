from setuptools import setup, find_packages

setup (
    name             = "app2",
    version          = "0.1",
    description      = "Example application to read ISS coordinates and publish an URL to street maps.",
    packages         = find_packages(),
    install_requires = ["gunicorn"],
)
